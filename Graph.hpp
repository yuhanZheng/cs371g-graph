// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector
#include <unordered_set> // unordered_set
#include <set>
#include <stdlib.h>     // srand, rand
#include <time.h>       // time

// -----
// Graph
// -----

class Graph {
public:
    // ------
    // usings
    // ------

    using vertex_descriptor = int;
    using edge_descriptor   = std::pair<vertex_descriptor, vertex_descriptor>;

    using vertex_iterator    = std::vector<vertex_descriptor>::const_iterator;
    using edge_iterator      = std::set<edge_descriptor>::const_iterator;
    using adjacency_iterator = std::set<vertex_descriptor>::const_iterator;


    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

public:
    // --------
    // add_edge
    // --------

    /**
     * add an edge from source_v to target_v
     * @param source_v vertex_descriptor of source vertex
     * @param target_v vertex_descriptor of target vertex
     * @param g reference to graph object used
     * @return pair<edge_descriptor, bool> edge_descriptor represents the edge;
     *                                     bool is true if edge is successfully added, false if edge already exist
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor source_v, vertex_descriptor target_v, Graph& g) {
        // <your code>
        // check if both vertices exist; more vertices needs to be added if not
        vertex_descriptor v_needed = source_v > target_v ? (source_v + 1) : (target_v + 1);
        if(v_needed > g.num_v) {
            while(g.num_v < v_needed) {
                add_vertex(g);
            }
        }
        edge_descriptor ed = std::make_pair(source_v, target_v);
        // add new edge to the set if it's not already there
        bool b  = g.edges.insert(ed).second;
        // add mew adjency vertex for source_v
        if(b) {
            g.adj_lists[source_v].insert(target_v);
        }

        assert(g.valid());
        return std::make_pair(ed, b);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * add a vertex to the graph
     * @param g reference to graph object used
     * @return vertex_descriptor of the newly added vertex
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        // <your code>
        // add new element both to the adj_lists and the vertices set
        vertex_descriptor v = g.num_v;
        g.adj_lists.push_back(std::set<vertex_descriptor>());
        g.vertices.push_back(v);
        ++g.num_v;
        assert(g.valid());
        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * get the begin and end iterator to the set of adjacency vertices of vd
     * @param vd vertex_descriptor of the vertex whose adjacency set is retrieved
     * @return a pair of iterators: the begin and end iterator to the set of adjacency vertices of vd
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor vd, const Graph& g) {
        // <your code>
        // stop if vd is not in the graph
        if((vd >= g.num_v)) {
            assert(false);
        }
        adjacency_iterator b = g.adj_lists[vd].cbegin();
        adjacency_iterator e = g.adj_lists[vd].cend();
        return std::make_pair(b, e);
    }

    // ----
    // edge
    // ----

    /**
     * find the edge between two vertices in the graph
     * @param sv vertex_descriptor of vertex that is the source of the edge
     * @param dv vertex_descriptor of vertex that is the target of the edge
     * @param g reference to graph object used
     * @return pair<edge_descriptor, bool> the edge_descriptor of the edge asked for;
     *                                      bool is true if edge is found and false otherwise
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor sv, vertex_descriptor dv, const Graph& g) {
        // <your code>
        // if source vertex are not in the graph
        if((sv >= g.num_v)) {
            assert(false);
        }

        edge_descriptor ed = std::make_pair(sv, dv);
        bool            b  = true;
        // mark bool as false if the edge is not there, create an arbitrary edge descriptor 
        if(g.edges.find(ed) == g.edges.end()) {
            srand (time(NULL));
            int v1 = rand() % (g.num_v + 100);
            int v2 = rand() % (g.num_v + 100);
            ed = std::make_pair(v1, v2);
            b = false;
        }
        return std::make_pair(ed, b);
    }

    // -----
    // edges
    // -----

    /**
     * get the begin and end iterator to the set of all edges in the graph
     * @param g reference to graph object used
     * @return a pair of iterators: the begin and end iterator to the set of all edges in the graph
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        // <your code>
        edge_iterator b = g.edges.cbegin();
        edge_iterator e = g.edges.cend();
        return std::make_pair(b, e);
    }

    // ---------
    // num_edges
    // ---------

    /**
     * get the total number of edges in the graph
     * @param g reference to graph object used
     * @return the total number of edges in the graph
     */
    friend edges_size_type num_edges (const Graph& g) {
        // <your code>
        return g.edges.size();
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * get the total number of vertices in the graph
     * @param g reference to graph object used
     * @return the total number of vertices in the graph
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        // <your code>
        return g.vertices.size();
    }

    // ------
    // source
    // ------

    /**
     * get source vertex of an egde
     * @param g reference to graph object used
     * @return vertex_descriptor of the source vertex
     */
    friend vertex_descriptor source (edge_descriptor ed, const Graph&) {
        // <your code>
        return ed.first;
    }

    // ------
    // target
    // ------

    /**
     * get target vertex of an egde
     * @param g reference to graph object used
     * @return vertex_descriptor of the target vertex
     */
    friend vertex_descriptor target (edge_descriptor ed, const Graph&) {
        // <your code>
        return ed.second;
    }

    // ------
    // vertex
    // ------

    /**
     * get the nth vertex in the vertex list
     * @param n index of the vertex
     * @param g reference to graph object used
     * @return vertex_descriptor of the nth vertex in the graph's vertex list
     */
    friend vertex_descriptor vertex (vertices_size_type n, const Graph& g) {
        // <your code>
        //undefined behavior if n out of range
        if(n >= num_vertices(g)) {
            return n;
        }
        return g.vertices[n];
    }

    // --------
    // vertices
    // --------

    /**
     * get the begin and end iterator to the set of all vertices in the graph
     * @param g reference to graph object used
     * @return a pair of iterators: the begin and end iterator to the set of all vertices in the graph
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        // <your code>
        vertex_iterator b = g.vertices.cbegin();
        vertex_iterator e = g.vertices.cend();
        return std::make_pair(b, e);
    }

private:
    // ----
    // data
    // ----

    std::vector<std::set<vertex_descriptor>> adj_lists;
    std::vector<vertex_descriptor> vertices;
    std::set<edge_descriptor> edges;
    vertex_descriptor num_v = 0;

    // -----
    // valid
    // -----

    /**
     * check if the current graph has a valid structure
     * @return true if this is a valid graph; false otherwise
     */
    bool valid () const {
        // <your code>
        // check number of vertices stored are consistent
        if((adj_lists.size() != vertices.size()) || (num_v < (int)vertices.size())) {
            return false;
        }

        // check all pairs of edges stored are consistent
        int count = 0;
        for(int i = 0; i < (int)adj_lists.size(); i++) {
            adjacency_iterator b = adj_lists[i].begin();
            adjacency_iterator e = adj_lists[i].end();
            while(b != e) {
                edge_descriptor ed = std::make_pair(i, *b);
                ++count;
                if(edges.find(ed) == edges.end()) {
                    return false;
                }
                ++b;
            }
        }

        // check number of edges stored are consistent
        if(count != (int)edges.size()) {
            return false;
        }

        return true;
    }

public:
    // --------
    // defaults
    // --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

#endif // Graph_h
